from django.contrib import admin
from .models import Car, Expense

admin.site.register(Car)
admin.site.register(Expense)
# Register your models here.
