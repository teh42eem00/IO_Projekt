from django.apps import AppConfig


class CarBudgetConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'car_budget'
